export const UPDATE_CITY = 'UPDATE_CITY';

export const updateCity = (cityToQuery) => {
    return {
        type: UPDATE_CITY,
        cityToQuery
    };
}