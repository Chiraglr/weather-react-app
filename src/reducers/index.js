import {UPDATE_CITY} from '../actions/';
import { combineReducers } from 'redux';
import {citiesCollections, weatherPredictions} from '../mockData/index';
// import weatherPredictions from '../mockData';

const cityToQuery = (state = '',action) => {
    switch(action.type){
        case UPDATE_CITY:
            return action.cityToQuery;
        default:
            return state;
    }
}

const cities = (state = citiesCollections,action) => state;
const predictions = (state = weatherPredictions,action) => state;

export default combineReducers({
    cityToQuery,
    cities,
    predictions,
});