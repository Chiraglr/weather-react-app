import React,{ Component } from 'react';
import './Results.css';
import { connect } from 'react-redux';
import WeatherCard from '../../Components/WeatherCard/WeatherCard';
// import { faSun, faCloud, faCloudRain, faCloudShowersHeavy, faCloudSun, faBolt } from '@fortawesome/free-solid-svg-icons';
// import { FaSun, FaCloud, FaCloudRain, FaCloudShowersHeavy, FaCloudSun, FaBolt } from 'react-icons/fa';
import { WiDaySunny, WiCloudy, WiRain, WiDaySnowThunderstorm, WiDaySunnyOvercast, WiDayLightning } from 'react-icons/wi';
import { IconContext } from 'react-icons';

const mapStateToProps = state => {
  console.log('state');
  console.log(state);
  return {
    predictions: state.predictions,
    cities: state.cities,
    cityToQuery: state.cityToQuery
  }
}

class Results extends Component{
    render(){
        const {predictions,cities,cityToQuery} = this.props;
        var i,j;
        i = -1;
        let cityLowerCase = cityToQuery.toLowerCase();
        for(j=0;j<cities.length;j++){
            if(cities[j].toLowerCase()===cityLowerCase){
                i = j;
                break;
            }
        };
        // const icon = [<i class="fa fa-sun"></i>,<i class="fa fa-cloud"></i>,<i class="fa fa-cloud-rain"></i>,<i class="fa fa-cloud-showers-heavy"></i>,<i class="far fa-sun"></i>,<i class="fa fa-cloud-sun"></i>,<i class="fa fa-cloud-showers-heavy"></i>];
        // let icon = [FaSun, FaCloud, FaCloudRain, FaCloudShowersHeavy, FaSun, FaCloudSun, FaBolt];
        let sizeI = 100;
        let icon = [
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI }}>
            <WiDaySunny />
          </IconContext.Provider>,
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI }}>
            <WiCloudy />
          </IconContext.Provider>,
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI}}>
            <WiRain />
          </IconContext.Provider>,
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI }}>
            <WiDaySnowThunderstorm />
          </IconContext.Provider>,
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI }}>
            <WiDaySunny />
          </IconContext.Provider>,
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI }}>
            <WiDaySunnyOvercast />
          </IconContext.Provider>,
          <IconContext.Provider value={{ color: "#1e2432", size: sizeI }}>
            <WiDayLightning />
          </IconContext.Provider>
        ];
        const possibleWeather = ["Sunny","Cloudy","Light Drizzle","Very Heavy Rain","Clear Skies",
        "Few Clouds","Thunder Storm"];
        let weatherInfo = [];
        // icon = icon.map(svg => <FontAwesomeIcon icon={svg} />)
        if(i!==-1){
          for(j=0;j<7;j++){
            weatherInfo.push({
              temperature: (predictions[i][j]).temperature,
              weather: predictions[i][j].weather,
              icon: icon[possibleWeather.indexOf(predictions[i][j].weather)]
            });
              // weatherCard+=`<WeatherCard city={cityToQuery} temperature={predictions[i][{j}]}  weather={predictions[i][j].weather} icon={icon[possibleWeather.indexOf(predictions[i][j].weather)]}/>`;
          }
        }
        let today = new Date();
        weatherInfo = weatherInfo.map(info=> {
          today.setDate(today.getDate()+1);
          return <WeatherCard city={cityToQuery} temperature={info.temperature} weather={info.weather} icon={icon[possibleWeather.indexOf(info.weather)]} date={today.toLocaleDateString('en-GB')} day={today.getDay()}/>;
        });
        return (
          <div className="results">
            <div className="weatherContainer">
              { i!==-1 && weatherInfo } 
            </div>
          </div>
        );
    }
}

export default connect(
  mapStateToProps,
  null,
)(Results);