import React from 'react';
import Results from './Containers/Results/Results';
import SearchBar from './Components/SearchBar/SearchBar';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1 class="appTitle">Simple Weather App</h1>
      <SearchBar/>
      <Results/>
    </div>
  );
}

export default App;
