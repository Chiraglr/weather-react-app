import React,{ Component } from 'react';
import './WeatherCard.css';
import 'font-awesome/css/font-awesome.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faSun, faCloud, faCloudRain, faCloudShowersHeavy, faCloudSun, faBolt } from '@fortawesome/free-solid-svg-icons';
import { FaSun, FaCloud, FaCloudRain, FaCloudShowersHeavy, FaCloudSun, FaBolt } from 'react-icons/wi';

class WeatherCard extends Component{
    render(){
        const {city,temperature,icon,weather,date,day} = this.props;
        const days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        return (
          <div className="weatherCard">
              <h1 class="city">{city}</h1>
              <div class="temperatureContainer">
                <div class="graphicContainer">
                    <h1 class="temperature">{temperature}<sup>°C</sup></h1>
                    {icon}
                </div>
                <div class="dateContainer">
                    <h1 class="date">{date}</h1>
                    <h1>{days[day]}</h1>
                </div>
              </div>
              {/* <FontAwesomeIcon class="icon" size="fa-xs" icon={icon} /> */}
              <h1 class="weather">{weather}</h1>
          </div>
        );
    }
}

export default WeatherCard;