import React,{ Component } from 'react';
import './SearchBar.css';
import { connect } from 'react-redux';
import {updateCity} from '../../actions';
import 'font-awesome/css/font-awesome.min.css';
import Select from 'react-select'

const mapDispatchToProps = dispatch => {
    return {
        onCitySelect: city => {
            console.log(city);
            dispatch(updateCity(city))
        }
    }
}

const mapStateToProps = state => {
    return {
        cities: state.cities,
        cityToQuery: state.cityToQuery
    }
}

class SearchBar extends Component{
    handleChange = selectedOption => {
        const { onCitySelect } = this.props;
        if(selectedOption!==null)
          onCitySelect(selectedOption.value);
        else
          onCitySelect('');
      };
    render(){
        let { cities, cityToQuery } = this.props;
        console.log("cityToQuery");
        console.log(cityToQuery);
        let options = cities.map(city=>{
            return {
                value: city,
                label: city
            }
        });
        console.log("options");
        console.log(options);
        const customStyles = {
            control: base => ({
              ...base,
              height: 35,
              minHeight: 35,
              width: '29vw',
              minWidth: '356px'
            })
          };
        let value;
        if(cityToQuery!=='')
          value = {value: cityToQuery, label: cityToQuery};
        else
          value = '';
        return (
          <div className="searchBar">
              <Select
                isClearable={true}
                placeholder="Search for a city"
                value={value}
                onChange={this.handleChange}
                options={options}
                styles={customStyles}
            />
          </div>
        );
    }
}

  export default connect(
    mapStateToProps,
    mapDispatchToProps,
  )(SearchBar);